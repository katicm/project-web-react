import { normalize } from "normalizr";
import { MOVIES_FETCHED, MOVIE_CREATED, MOVIE_DELETED } from "../types";
import api from "../api";
import { movieSchema } from "../schemas";

// data.entities.movies
const moviesFetched = data => ({
  type: MOVIES_FETCHED,
  data
});

const movieCreated = data => ({
  type: MOVIE_CREATED,
  data
});

const movieDeleted = data => ({
  type: MOVIE_DELETED,
  data
});
//ubaciti dispatch
export const fetchMovies = () => dispatch =>
  api.movies
    .fetchAll()
    .then(movies => dispatch(moviesFetched(normalize(movies, [movieSchema]))));
//normalize pomaze pri citanju jsona, vraca kao dictionaries
export const createMovie = data => dispatch =>
  api.movies
    .create(data)
    .then(movie => dispatch(movieCreated(normalize(movie, movieSchema))));

export const deleteMovie = id => dispatch =>
  api.movies
    .delete(id)
    .then(movie => dispatch(movieDeleted(normalize(movie, movieSchema))));

export const getCredits = id => () => api.movies.credits(id);

export const searchMovies = data => () => api.movies.search(data);
