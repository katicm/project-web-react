import axios from "axios";

export default {
  user: {
    login: credentials =>
      axios.post("/api/auth", { credentials }).then(res => res.data.user),
    signup: user =>
      axios.post("/api/users", { user }).then(res => res.data.user),
    confirm: token =>
      axios
        .post("/api/auth/confirmation", { token })
        .then(res => res.data.user),
    resetPasswordRequest: email =>
      axios.post("/api/auth/reset_password_request", { email }),
    validateToken: token => axios.post("/api/auth/validate_token", { token }),
    resetPassword: data => axios.post("/api/auth/reset_password", { data })
  },
  movies: {
    fetchAll: () => axios.get("/api/movies").then(res => res.data.movies),
    create: movie =>
      axios.post("/api/movies", { movie }).then(res => res.data.movie),
    search: data =>
      axios.get(`/api/movies/search?q=${data}`).then(res => res.data.movies),
    delete: id =>
      axios.post("/api/movies/delete", { id }).then(res => res.data.movie),
    credits: id =>
      axios.get(`/api/movies/credits?q=${id}`).then(res => res.data.credits)
  }
};
