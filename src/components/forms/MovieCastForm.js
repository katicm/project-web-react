import React from "react";
import { Grid, Icon } from "semantic-ui-react";
import ModalImage from "react-modal-image";
import no_image from "../media/no_image.png";

class MovieCastForm extends React.Component {
  castTable = movie => {
    var table = [];
    for (let i = 0; i < movie.actors.length; i++) {
      let box = [];
      box.push(
        <Grid.Column>
          <ModalImage
            small={
              movie.actors[i].profile_picture === null
                ? no_image
                : movie.actors[i].profile_picture
            }
            large={
              movie.actors[i].profile_picture === null
                ? no_image
                : movie.actors[i].profile_picture.replace("w92", "w500")
            }
            alt={movie.actors[i].name}
          />
          <p>
            <label>
              <b>{movie.actors[i].name}</b>
              <Icon name={movie.actors[i].gender === 2 ? "man" : "woman"} />
            </label>
            <br />
            <font size="2"> as {movie.actors[i].character}</font>
          </p>
        </Grid.Column>
      );
      table.push(box);
    }
    return table;
  };
  render() {
    const { movie } = this.props;
    return (
      <Grid columns={3}>
        <Grid.Column>
          <ModalImage
            small={
              movie.director.profile_picture !== null
                ? movie.director.profile_picture
                : no_image
            }
            large={
              movie.director.profile_picture !== null
                ? movie.director.profile_picture.replace("w92", "w500")
                : no_image
            }
          />
          <p>
            <font size="2">Directed by</font>
            <br />
            <label>
              <b>{movie.director.name}</b>
              <Icon name={movie.director.gender === 2 ? "man" : "woman"} />
            </label>
          </p>
        </Grid.Column>
        {this.castTable(movie)}
      </Grid>
    );
  }
}
export default MovieCastForm;
