import React, { Component } from "react";
import { Dropdown, Form } from "semantic-ui-react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { searchMovies, getCredits } from "../../actions/movies";

class SearchMoviesForm extends Component {
  state = {
    query: "",
    loading: false,
    options: [],
    movies: {}
  };
  onSearchChange = (e, data) => {
    clearTimeout(this.timer);
    this.setState({
      query: data.searchQuery
    });
    this.timer = setTimeout(this.fetchOptions, 500);
  };
  //movie selected
  onChange = (e, data) => {
    this.setState({ query: data.value }); //data.value id movie from tMDB
    this.props.getCredits(data.value).then(credits => {
      this.props.onMovieSelect({
        ...this.state.movies[data.value],
        ...credits
      });
    });
  };
  fetchOptions = () => {
    if (!this.state.query) return;
    this.setState({ loading: true });
    this.props.searchMovies(this.state.query).then(movi => {
      const options = [];
      const moviesHash = {};
      movi.forEach(movie => {
        moviesHash[movie.tMDBId] = movie;
        options.push({
          text: movie.title,
          value: movie.tMDBId,
          image: { avatar: true, src: movie.cover }
        });
      });
      this.setState({ loading: false, options, movies: moviesHash });
    });
  };

  render() {
    return (
      <Form>
        <Dropdown
          search
          fluid
          placeholder="Search for a movie by title"
          value={this.state.query}
          onSearchChange={this.onSearchChange}
          options={this.state.options}
          loading={this.state.loading}
          onChange={this.onChange}
        />
      </Form>
    );
  }
}
SearchMoviesForm.propTypes = {
  onMovieSelect: PropTypes.func.isRequired,
  searchMovies: PropTypes.func.isRequired
};

export default connect(
  null,
  { searchMovies, getCredits }
)(SearchMoviesForm);
