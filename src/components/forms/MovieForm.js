import React from "react";
import PropTypes from "prop-types";
import ModalImage from "react-modal-image";
import MovieCastForm from "../forms/MovieCastForm";
import {
  Form,
  Button,
  Grid,
  Segment,
  Image,
  Rating,
  Label,
  Icon,
  GridColumn
} from "semantic-ui-react";

class MovieForm extends React.Component {
  state = {
    loading: false
  };

  onChange = e =>
    this.setState({
      ...this.state,
      data: { ...this.state.data, [e.target.name]: e.target.value }
    });

  onChangeNumber = e =>
    this.setState({
      ...this.state,
      data: {
        ...this.state.data,
        [e.target.name]: parseInt(e.target.value, 10)
      }
    });

  onSubmit = e => {
    e.preventDefault();
    this.setState({ loading: true });
    this.props
      .submit(this.props.movie)
      .catch(err =>
        this.setState({ errors: err.response.data.errors, loading: false })
      );
  };
  onRate = (e, { rating }) => {
    this.props.changeRate(rating);
  };

  render() {
    const { movie } = this.props;
    return (
      <Segment basic>
        <Form onSubmit={this.onSubmit} loading={this.state.loading}>
          <Grid columns={2} stackable>
            <Grid.Column verticalAlign={"top"}>
              <Segment color="teal">
                <Grid columns={2} stackable>
                  <GridColumn width={4}>
                    <label>Movie Title:</label>
                  </GridColumn>
                  <GridColumn width={12}>
                    <h3>{movie.title}</h3>
                  </GridColumn>
                </Grid>
              </Segment>
              <Segment.Group horizontal>
                <Segment color="teal">
                  <label>User ratings: </label>
                  <Rating
                    icon="star"
                    defaultRating={movie.score}
                    maxRating={10}
                    size="small"
                    onRate={this.onRate}
                  />
                </Segment>
                <Segment textAlign="center" color="teal">
                  <label>Release date:</label>
                  <Label>
                    <Icon name="calendar alternate" />
                    {movie.release_date}
                  </Label>
                </Segment>
              </Segment.Group>

              <Segment color="teal">
                <MovieCastForm movie={movie} />
              </Segment>
              <Button primary>Submit</Button>
            </Grid.Column>

            <Grid.Column>
              <Segment color="teal" textAlign="center">
                {movie.overview}
              </Segment>
              <Segment basic textAlign="center">
                <Image size="medium">
                  <ModalImage
                    small={movie.cover}
                    large={movie.cover}
                    alt={movie.title}
                  />
                </Image>
              </Segment>
            </Grid.Column>
          </Grid>
        </Form>
      </Segment>
    );
  }
}

MovieForm.propTypes = {
  submit: PropTypes.func.isRequired,
  changeRate: PropTypes.func.isRequired,
  movie: PropTypes.shape({
    tMDBId: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    score: PropTypes.number.isRequired,
    cover: PropTypes.string.isRequired,
    original_title: PropTypes.string.isRequired,
    release_date: PropTypes.string.isRequired,
    overview: PropTypes.string.isRequired
  }).isRequired
};

export default MovieForm;
