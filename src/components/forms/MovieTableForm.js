import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import ModalImage from "react-modal-image";
import { deleteMovie } from "../../actions/movies";
import { Grid, Rating, Icon, Label, Button, Divider } from "semantic-ui-react";
import { allMoviesSelector } from "../../reducers/movies";

class MovieTable extends Component {
  createTable = () => {
    var table = [];
    for (let i = this.props.movies.length - 1; i >= 0; i--) {
      let row = [];
      row.push(
        <Grid.Row>
          <Grid.Column verticalAlign="middle" width={3}>
            <ModalImage
              small={this.props.movies[i].cover}
              large={this.props.movies[i].cover}
              alt={this.props.movies[i].title}
            />
          </Grid.Column>
          <Grid.Column width={12} verticalAlign="middle">
            <Grid celled="internally">
              <Grid.Row>
                <Grid.Column width={4}>Movie name:</Grid.Column>
                <Grid.Column width={10}>
                  {this.props.movies[i].title}
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column width={4}>Movie ratings:</Grid.Column>
                <Grid.Column>
                  <Rating
                    disabled
                    icon="star"
                    defaultRating={this.props.movies[i].score}
                    maxRating={10}
                    size="small"
                  />
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column width={4}>Release date:</Grid.Column>
                <Grid.Column width={10}>
                  <Label onChange={this.onChange}>
                    <Icon name="calendar alternate" />
                    {this.props.movies[i].release_date}
                  </Label>
                </Grid.Column>
              </Grid.Row>
              <Grid.Row>
                <Grid.Column width={4}>Storyline:</Grid.Column>
                <Grid.Column width={12}>
                  {this.props.movies[i].overview}
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Grid.Column>
          <Grid.Column width={1}>
            <Button
              id={this.props.movies[i]._id.toString()}
              onClick={this.onClickDelete}
              circular
              icon="remove circle"
              negative
            />
            <Divider />
            <Button
              id={this.props.movies[i]._id.toString()}
              onClick={this.onClick}
              circular
              icon="info circle"
              positive
            />
          </Grid.Column>
        </Grid.Row>
      );
      table.push(row);
    }
    return table;
  };
  onClick = () => {
    console.log("aaa");
  };
  onClickDelete = e => {
    this.props.deleteMovie(e.target.id);
  };
  render() {
    return (
      <div>
        <Grid celled="internally">{this.createTable()}</Grid>
      </div>
    );
  }
}

MovieTable.PropTypes = {
  movies: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired
    }).isRequired
  ).isRequired
};

function mapStateToProps(state) {
  return {
    movies: allMoviesSelector(state)
  };
}

export default connect(
  mapStateToProps,
  { deleteMovie }
)(MovieTable);
