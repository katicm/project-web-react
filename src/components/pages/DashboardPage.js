import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import ConfirmEmailMessage from "../messages/ConfirmEmailMessage";
import AddMovie from "../ctas/AddMovie";
import { fetchMovies } from "../../actions/movies";
import { Divider } from "semantic-ui-react";
import MovieTableForm from "../forms/MovieTableForm";

class DashboardPage extends React.Component {
  componentDidMount = () => this.onInit(this.props);
  onInit = props => props.fetchMovies();
  render() {
    const { isConfirmed } = this.props;
    return (
      <div>
        {!isConfirmed && <ConfirmEmailMessage />}

        <div>
          <MovieTableForm />
          <Divider section />
          <AddMovie />
        </div>
      </div>
    );
  }
}

DashboardPage.propTypes = {
  isConfirmed: PropTypes.bool.isRequired,
  fetchMovies: PropTypes.func.isRequired
};

function mapStateToProps(state) {
  return {
    isConfirmed: !!state.user.confirmed
  };
}

export default connect(
  mapStateToProps,
  { fetchMovies }
)(DashboardPage);
