import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Segment } from "semantic-ui-react";
import SearchMovieForm from "../forms/SearchMovieForm";
import MovieForm from "../forms/MovieForm";
import { createMovie } from "../../actions/movies";

class NewMoviePage extends Component {
  state = {
    movie: null
  };
  onMovieSelect = movie => {
    this.setState({ movie });
  };
  addMovie = movie =>
    this.props
      .createMovie(movie)
      .then(() => this.props.history.push("/dashboard"));

  changeRate = rating => {
    this.setState({
      movie: { ...this.state.movie, score: rating }
    });
  };
  render() {
    return (
      <Segment color="black">
        <h1>Add new movie to your collection</h1>
        <Segment color="grey">
          <SearchMovieForm onMovieSelect={this.onMovieSelect} />
        </Segment>

        {this.state.movie && (
          <MovieForm
            changeRate={this.changeRate}
            submit={this.addMovie}
            movie={this.state.movie}
          />
        )}
      </Segment>
    );
  }
}

NewMoviePage.propTypes = {
  createMovie: PropTypes.func.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired
};
export default connect(
  null,
  { createMovie }
)(NewMoviePage);
