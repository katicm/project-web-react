import React from "react";
import PropTypes from "prop-types";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import * as actions from "../../actions/auth";
import { Segment, Button, Divider, Image } from "semantic-ui-react";
import logo from "../media/anima.gif";

const HomePage = ({ isAuthenticated, logout }) => (
  <div>
    {isAuthenticated ? (
      <Redirect to="/dashboard"/>
    ) : (
      <div>
        <Image src={logo} />
        <Segment padded>
          <Link to="login">
            <Button primary fluid>
              Login
            </Button>
          </Link>
          <Divider horizontal>Or</Divider>
          <Link to="/signup">
            <Button secondary fluid>
              Sign Up Now
            </Button>
          </Link>
        </Segment>
      </div>
    )}
  </div>
);

HomePage.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  logout: PropTypes.func.isRequired
};

function mapStateToProps(state) {
  return {
    isAuthenticated: !!state.user.token
  };
}

export default connect(
  mapStateToProps,
  { logout: actions.logout }
)(HomePage);
