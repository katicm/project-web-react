import React from "react";
import { Card, Icon } from "semantic-ui-react";
import { Link } from "react-router-dom";

const AddMovie = () => (
  <Card centered>
    <Card.Content textAlign="center">
      <Card.Header>Add new movie</Card.Header>
      <Link to="/movies/new">
        <Icon name="plus circle" size="massive" />
      </Link>
    </Card.Content>
  </Card>
);

export default AddMovie;
