import React from "react";
import PropTypes from "prop-types";
import { Menu, Dropdown, Image, Icon, MenuItem } from "semantic-ui-react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { allMoviesSelector } from "../../reducers/movies";
//import gravatarUrl from "gravatar-url";
import * as auth from "../../actions/auth";
import avatar from "../media/avatar.png";
import kmavatar from "../media/kmavatar.png";

const TopNavigation = ({ logout, hasMovies }) => (
  <Menu secondary pointing>
    <Menu.Item as={Link} to="/dashboard">
      <img src={kmavatar} alt="kmavatar.png is missing!" />
    </Menu.Item>
    <Menu.Item as={Link} to="/dashboard">
      <Icon name="video play" />
      Collections
    </Menu.Item>

    {hasMovies && (
      <Menu.Item as={Link} to="/movies/new">
        <Icon name="add" />
        Add New Movie
      </Menu.Item>
    )}
    <Menu.Menu position="right">
      <MenuItem>
        <Dropdown trigger={<Image avatar src={avatar} />}>
          <Dropdown.Menu>
            <Dropdown.Item onClick={() => logout()}>Logout</Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      </MenuItem>
    </Menu.Menu>
  </Menu>
);
TopNavigation.propTypes = {
  user: PropTypes.shape({
    email: PropTypes.string.isRequired
  }).isRequired,
  hasMovies: PropTypes.bool.isRequired,
  logout: PropTypes.func.isRequired
};
function mapStateToProps(state) {
  return {
    user: state.user,
    hasMovies: allMoviesSelector(state).length > 0
  };
}

export default connect(
  mapStateToProps,
  { logout: auth.logout }
)(TopNavigation);
