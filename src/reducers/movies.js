import { createSelector } from "reselect";
import { MOVIES_FETCHED, MOVIE_CREATED, MOVIE_DELETED } from "../types";

export default function movies(state = {}, action = {}) {
  switch (action.type) {
    case MOVIES_FETCHED:
    case MOVIE_CREATED:
      return { ...state, ...action.data.entities.movies }; //pun state se overrajduje ili dodaje drugi clan
    case MOVIE_DELETED: {
      return { ...state, ...delete state[action.data.result] };
    }
    default:
      return state;
  }
}
//selector pretvara niz u object?????
export const moviesSelector = state => state.movies;

export const allMoviesSelector = createSelector(moviesSelector, moviesHash =>
  Object.values(moviesHash)
);
